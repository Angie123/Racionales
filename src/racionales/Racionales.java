/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package racionales;

/**
 * Clase encargada de representar a los numeros racionales.
 * @author Angie Reyes
 */
public class Racionales {
       
    // Variable que representa al numerador.
    int p;
    // Variable que representa al denominador.
    int q;
    
    /*
     * Constructor por default (Sin parametros).
    */
    Racionales(){
        this.p = 1;
        this.q = 1;
    }
    
    /*
     * Constructor por parametros.
    */
    Racionales(int p, int q){
        this.p = p;
        this.q = q;
    }
    
    /**
     * Metodo que asigna un nuevo valor al numerador.
     * @param p El nuevo valor del numerador.
     */
    private void setNumerador(int p){
        this.p = p;
    }
    
    /**
     * Metodo que asigna un nuevo valor al denominador.
     * @param q El nuevo valor del denominador.
     */
    private void setDenominador(int q){
        this.q = q;
    }
    
    /**
     * Metodo que regresa el valor del numerador.
     * @return El valor del numerador.
     */
    private int getNumerador(){
        return this.p;
    }
    
    /**
     * Metodo que regresa el valor del denominador.
     * @return El valor del denominador.
     */
    private int getDenominador(){
        return this.q;
    }
    
    /**
     * Metodo que imprime el numero racional.
     */
    void show(){
        System.out.println(getNumerador() + "/" + getDenominador());
    }
    
    /**
     * Metodo que realiza la multiplicacion de dos numeros racionales.
     * @param r El numero racional con el que multiplicaremos.
     * @return El resultado de la multiplicacion.
     */
    Racionales multiplicaRacionales(Racionales r){
        int p = 0;
        int q = 1;
        
       
        
        p = getNumerador() * r.getNumerador();
        q = getDenominador() * r.getDenominador();
        
        
       
        
        Racionales a=new Racionales(p,q);
        return(a.Simplifica());
        
    }
 
    
    Racionales divideRacionales(Racionales r){
        int p = 0;
        int q = 1;
        
        p = getNumerador() * r.getDenominador();
        q = getDenominador() * r.getNumerador();
        
        Racionales a=new Racionales(p,q);
        return(a.Simplifica());
        
    }
    
    //SumaRacionales y RestaRacionales
    //Definimos mcd
    private int mcd(int n1, int n2){
        int mcd=0;
        int a=0;
        int b=0;
        
        a=Math.max(n1,n2);
        b=Math.min(n1,n2);
        
        while(b!=0){
            mcd=b;
            b=a%b;
            a=mcd;
        }
        return mcd;
    }
    //Primero calculamos el mcm
    private int mcm(int n1, int n2){
        int mcm=0;
        int a=0;
        int b=0;
        
        a=Math.max(n1,n2);
        b=Math.min(n1,n2);
        mcm=(a/mcd(a,b))*b;
        
        return mcm;
    }
    Racionales sumaRacionales (Racionales r){
        int p=0;
        int q=1;
        
        q=mcm(getDenominador(),r.getDenominador());
        p=q*getNumerador()/getDenominador()+(q*r.getNumerador()/r.getDenominador());
        
        
        Racionales a=new Racionales(p,q);
        return(a.Simplifica());
        
    }
    Racionales restaRacionales( Racionales r){
        int p=0;
        int q=1;
        
        q=mcm(getDenominador(),r.getDenominador());
        p=q*getNumerador()/getDenominador()-(q*r.getNumerador()/r.getDenominador());
        
       
        Racionales a=new Racionales(p,q);
        return(a.Simplifica());
        
        
        
    }
    //simplificación de racionales
    Racionales Simplifica (){
        int p,q, mcd;
        mcd=mcd(this.getNumerador(), this.getDenominador());
        p=this.getNumerador()/mcd;
        q=this.getDenominador()/mcd;
        
        return( new Racionales(p,q));
        
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Creamos dos objetos que representaran a dos numeros racionales.
        Racionales r1 = new Racionales(15, 30);
        Racionales r2 = new Racionales(30, 45);
        
        // Declaramos un objeto 
        Racionales r3;
        
        // Imprimimos r1:
        System.out.print("R1 es: ");
        r1.show();
        
        // Imprimimos r2:
        System.out.print("R2 es: ");
        r2.show();
        
        // En r3 guardaremos el resultado.
        // MULTIPLICACION:
        r3 = r1.multiplicaRacionales(r2);
        
        // Imprimimos el resultado.
        System.out.print("El resultado de la multiplicacion es: ");
        r3.show();
        
        r3 = r1.divideRacionales(r2);
        
        // Imprimimos el resultado.
        System.out.print("El resultado de la division es: ");
        r3.show();
        
        //Imprimimos el resultado de la suma
        r3.show();
        r3=r1.sumaRacionales(r2);
        
        //Imprimimos el resultado.
        System.out.print ("El resultado de la suma es:" );
        r3.show();
        
        //Imprimimos el resultado de la resta
        r3.show();
        r3=r1.restaRacionales(r2);
        
        //Imprimimos el resultado.
        System.out.print ("El resultado de la resta es:" );
        r3.show();
        
        
        
        
        
    }
    
}


    
                
        
        
    

